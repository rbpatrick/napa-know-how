<?php 

$php_includes = dirname($_SERVER['DOCUMENT_ROOT'], 1).DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR;
$srv_name = $_SERVER['SERVER_NAME'];

require_once($php_includes.'functions.php');

if (isset($_GET["p"])) {
	$page = $_GET["p"];
}
else {
	$page = 'home';
}

switch ($page) {
case 'home':
	render('templates/meta');
	render('templates/header');
	render($page);
	render('templates/footer');
	render('templates/eof');
	break;
case 'customers':
	render('templates/meta');
	render('templates/header');
	render($page);
	render('templates/footer');
	render('templates/eof');
	break;
default:
	render('templates/meta');
	render('templates/header');
	render('404');
	render('templates/footer');
	render('templates/eof');
	break;

}
