<?php

/***
 * PHP functions
 */

function secure_assoc($data) {
	foreach($data as $key => &$value) {
		$value = htmlspecialchars($value);
	}
}

function render($template, $data = array()) {
	$path = '..'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR;

	if (file_exists($path.$template.'.php')) {
		$view = $path.$template.'.php';

		secure_assoc($data);

		extract($data);
		require($view);
	}
	else {
		echo 'View not found!';
	}
}
