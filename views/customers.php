<main>

<?php 

require '../includes/db_connection.php';

$sql	= "SELECT * FROM customers";
$result	= mysqli_query($conn, $sql);

?>

<table>
<thead>
	<tr>
		<th>ID</th>
		<th>Customer</th>
		<th>Priority</th>
		<th>Street</th>
		<th>City</th>
		<th>State</th>
		<th>Zip</th>
	</tr>
</thead>
<tbody>
<?php

if (mysqli_num_rows($result) > 0) {
	// Output data of each row
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<tr> \r\n"
			. "<td>" . $row["customer_id"] . "</td> \r\n"
			. "<td>" . $row["customer"] . "</td> \r\n"
			. "<td>" . $row["priority"] . "</td> \r\n"
			. '<td><a target="_blank" href="https://www.google.com/maps/place/'.$row['street'].' '.$row['city'].' '.$row['state'].' '.$row['zip'].'">'.$row['street']."</a></td> \r\n"
			. "<td>" . $row["city"] . "</td> \r\n"
			. "<td>" . $row["state"] . "</td> \r\n"
			. "<td>" . $row["zip"] . "</td> \r\n"
			. "</tr> \r\n";
	}
} else {
	echo "0 results";
}

?>
</tbody>
</table>
<?php mysqli_close($conn); ?>
</main>
