<main>

<p>This web page is a simple proof of concept which, so far, accomplishes a successful connection between the web server and database with the server-side programming language known as PHP.</p>

<p>Below is a database SELECT statment which pulls information from the database and displays it on the web page in whatever way it is programmed to display. For now, it is set to display the data in a table in ascending order based on the database record's ID.</p>

<?php 
require('../includes/db_connection.php');
$sql	= "SELECT * FROM customers";
$result	= mysqli_query($conn, $sql);

?>

<table>
<thead>
	<tr>
		<th>ID</th>
		<th>Customer</th>
		<th>Priority</th>
		<th>Street</th>
		<th>City</th>
		<th>State</th>
		<th>Zip</th>
	</tr>
</thead>
<tbody>
<?php

if (mysqli_num_rows($result) > 0) {
	// Output data of each row
	while ($row = mysqli_fetch_assoc($result)) {
		echo "<tr> \r\n"
			. "<td>" . $row["customer_id"] . "</td> \r\n"
			. "<td>" . $row["customer"] . "</td> \r\n"
			. "<td>" . $row["priority"] . "</td> \r\n"
			. "<td>" . $row["street"] . "</td> \r\n"
			. "<td>" . $row["city"] . "</td> \r\n"
			. "<td>" . $row["state"] . "</td> \r\n"
			. "<td>" . $row["zip"] . "</td> \r\n"
			. "</tr> \r\n";
	}
} else {
	echo "0 results";
}
mysqli_free_result($result);
mysqli_close($conn);
?>
</tbody>
</table>
</main>
