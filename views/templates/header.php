<?php 
include('../includes/db_connection.php');
$sql = 'SELECT * FROM subjects';
$result = mysqli_query($conn, $sql);
?>
<header>
	<h1>Napa Know-How</h1>
	<nav>
		<ul>
		<?php
			if (mysqli_num_rows($result) > 0) {
				while ($row = mysqli_fetch_assoc($result)) {
					echo '<li><a href="?p='.strtolower($row['subject']).'">'.$row['subject'].'</a></li>';
				}
			}
			else {
				echo '0 results';
			}
		?>
		</ul>
	</nav>
</header>
<?php
mysqli_free_result($result);
mysqli_close($conn);
?>
